/*
* step 2
*/ 
-- there can be many DI items per CR:
-- select CUST_REQUEST_ID, count(*) from IRCS.CUSTREQ_MBOM_DETAILS
-- where MBOM_ITEM_TYPE_NAME like 'DI-%'
-- group by CUST_REQUEST_ID
-- in order to link IRCS-CR to FTME-WR, filter down the list of WR
-- all rows have exactly one of: FC Number or non-zero TPRN number
-- logic is different for matching TPRNs and FCs
-- the "top" matching work request from FTME, by each TPRN number and FC+airplane combo
-- TPRN Rule: 
-- Complete Early
-- Complete Late
-- Partial Late
-- Partial Early

if object_id('IbpLoad.ftme_top_workrequests') is not null drop table IbpLoad.ftme_top_workrequests;
SELECT *
into IbpLoad.ftme_top_workrequests
FROM (
       SELECT *, row_number() over (
             partition by TPRNNumber order by 
             coalesce(CompleteStatus, 'Z') asc, -- sort so that C > P > NULL (coalesce NULL to Z to sort last)
             case when CompleteStatus = 'P' then datediff(mi, StartDate, '2010-01-01')  -- subtract 2010 (before earliest startdate) from startdate so that later times are lower (negative and larger magnitude)
                    else datediff(mi, '2010-01-01', StartDate)  -- subtract startdate from 2010 so that earlier times are lower (positive and smaller magnitude)
             end asc
       ) as [rank]
       FROM [FTME].[WorkRequests]
       where TPRNNumber is not NULL and TPRNNumber > 0 -- some with an FC number have a TPRN of zero
) ranked where [rank] = 1;

-- FC rule: earliest start date
insert into IbpLoad.ftme_top_workrequests
SELECT *
FROM (
       SELECT *, row_number() over (
             partition by FlightConfigurationNumber, AirplaneNoFCInbox order by StartDate asc
       ) as [rank]
       FROM [FTME].[WorkRequests]
       where FlightConfigurationNumber is not NULL 
) ranked where [rank] = 1;

-- only match IRCS to complete FTME work requests (complete = stageorder 6)
delete from IbpLoad.ftme_top_workrequests where StageOrder != 6;

-- get rid of leading "LTPR #" stuff
update IbpLoad.ftme_top_workrequests set FlightConfigurationNumber = substring(FlightConfigurationNumber, 5, len(FlightConfigurationNumber)) where FlightConfigurationNumber like 'LTPR%'
update IbpLoad.ftme_top_workrequests set FlightConfigurationNumber = LTRIM(RTRIM(REPLACE(FlightConfigurationNumber, '#', '')));

-- join IRCS back to FTME (step 3 to 2)
--take just one CR+LINE_NO 
--if object_id('IbpLoad.ircs_ftme_matches_step3_to_step2') is not null drop table IbpLoad.ircs_ftme_matches_step3_to_step2;
WITH CTE_IRCS_FTME_LINK (Work_Request,Cust_Request_Id)
as
(
Select WORK_REQUEST,
       LEFT(SUBSTRING(REF_NO, PATINDEX('%[0-9.--]%', REF_NO), 8000),
       PATINDEX('%[^0-9.--]%', SUBSTRING(REF_NO, PATINDEX('%[0-9.--]%', REF_NO), 8000) + 'X') -1) Cust_Request_Id
from FTME.WBS_DETAILS
Where REF_NO Like '%CR%' and LEFT(SUBSTRING(REF_NO, PATINDEX('%[0-9.--]%', REF_NO), 8000),
           PATINDEX('%[^0-9.--]%', SUBSTRING(REF_NO, PATINDEX('%[0-9.--]%', REF_NO), 8000) + 'X') -1) not in (' ') 
)
Insert Into IbpLoad.ircs_ftme_matches_step3_to_step2(CUST_REQUEST_ID,LINE_NO,WorkRequestNumber)
Select CTE.CUST_REQUEST_ID
      ,ircs_di_items_final.LINE_NO
         ,Max(CTE.Work_Request) As WorkRequestNumber
From CTE_IRCS_FTME_LINK CTE
Inner Join IbpLoad.ircs_di_items_final on ircs_di_items_final.Cust_Request_Id = Cte.Cust_Request_Id 
Left Join IbpLoad.ftme_top_workrequests 
   on ftme_top_workrequests.AirplaneNoFCInbox = ircs_di_items_final.AP_EFF_CODE
   --and Cte.Work_Request = ftme_top_workrequests.WorkRequestNumber
Where ircs_di_items_final.[REQUEST_AUTH_TYPE_NAME] In ('FC','LTPR') 
  and cast(Cte.CUST_REQUEST_ID as varchar) + '-' + cast(ircs_di_items_final.LINE_NO as varchar) not in (select cast(CUST_REQUEST_ID as varchar) + '-' + cast(LINE_NO as varchar) from IbpLoad.ircs_ftme_matches_step3_to_step2)
Group By CTE.CUST_REQUEST_ID,ircs_di_items_final.LINE_NO;

-- update IRCS CC with the FTME CC where matched
-- TODO this can possibly be retired once the realignment to GOLD-first is done
update ircs
set AP_CONTROL_CODE = ftme.CONTROLCODE
from IbpLoad.ircs_di_items_final ircs
inner join IbpLoad.ircs_ftme_matches_step3_to_step2
  on ircs.CUST_REQUEST_ID = ircs_ftme_matches_step3_to_step2.CUST_REQUEST_ID and ircs.LINE_NO = ircs_ftme_matches_step3_to_step2.LINE_NO
inner join IbpLoad.ftme_top_workrequests ftme 
  on ircs_ftme_matches_step3_to_step2.WorkRequestNumber = ftme.WorkrequestNumber
where ftme.WorkrequestNumber is not null


--Drop table IbpLoad.ircs_ftme_matches_step3_to_step2

